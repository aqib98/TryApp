/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Switch,
  Image,
  PixelRatio,
  KeyboardAvoidingView,
  ScrollView,
  TouchableHighlight,
  TouchableOpacity,
  TextInput

} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import Icon from 'react-native-vector-icons/FontAwesome';
import { StackNavigator } from 'react-navigation';


const Dimensions = require('Dimensions');
const {height, width} = Dimensions.get('window');


import Home from './source/components/home.js'
import SignUp from './source/components/signup.js'
import SignIn from './source/components/signin.js'
import Workout from './source/components/workout.js'
import UpcomingWorkouts from './source/components/upcomingWorkouts.js'
import UpcomingWorkouts1 from './source/components/upcomingWorkouts1.js'
import UpcomingWorkouts2 from './source/components/upcomingWorkouts2.js'
import UpcomingWorkouts3 from './source/components/upcomingWorkouts3.js'
import UpcomingWorkouts4 from './source/components/upcomingWorkouts4.js'
import UpcomingWorkouts5 from './source/components/upcomingWorkouts5.js'
import UpcomingWorkouts6 from './source/components/upcomingWorkouts6.js'
import UpcomingWorkouts7 from './source/components/upcomingWorkouts7.js'
import UpcomingWorkouts8 from './source/components/upcomingWorkouts8.js'
import UpcomingWorkouts9 from './source/components/upcomingWorkouts9.js'
import UpcomingWorkouts10 from './source/components/upcomingWorkouts10.js'
import UpcomingWorkouts11 from './source/components/upcomingWorkouts11.js'
import AllComponents from './source/components/allComponents.js'
import AreaChartExample from './source/components/graph.js'

console.disableYellowBox = true;

const App = StackNavigator({

  Home: { screen: Home },
  SignIn : {screen : SignIn},
  SignUp : { screen : SignUp},
  Workout : { screen : Workout},
  UpcomingWorkouts : { screen : UpcomingWorkouts},
  UpcomingWorkouts1 : { screen : UpcomingWorkouts1},
  UpcomingWorkouts2 : { screen : UpcomingWorkouts2},
  UpcomingWorkouts3 : { screen : UpcomingWorkouts3},
  UpcomingWorkouts4 : { screen : UpcomingWorkouts4},
  UpcomingWorkouts5 : { screen : UpcomingWorkouts5},
  UpcomingWorkouts6 : { screen : UpcomingWorkouts6},
  UpcomingWorkouts7 : { screen : UpcomingWorkouts7},
  UpcomingWorkouts8 : { screen : UpcomingWorkouts8},
  UpcomingWorkouts9 : { screen : UpcomingWorkouts9},
  UpcomingWorkouts10 : { screen : UpcomingWorkouts10},
  UpcomingWorkouts11 : { screen : UpcomingWorkouts11},
  AllComponents : { screen : AllComponents},
  AreaChartExample : { screen : AreaChartExample}

},{
  initialRouteName: 'Home',
  headerMode: 'none',
});

export default App
