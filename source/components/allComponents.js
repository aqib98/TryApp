import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  Button,
  TouchableHighlight,
  TouchableOpacity,
  ScrollView,
  Platform
} from 'react-native';
import {TabNavigator,TabBarBottom  } from 'react-navigation';
import Icon from 'react-native-vector-icons/FontAwesome'

import UpcomingWorkouts from './upcomingWorkouts.js'
import styles from './styles.js'

const MyNavScreen = ({ navigation}) => (
<ScrollView >
    <Text>hi</Text>
  </ScrollView>
);

const Feed = ({ navigation }) => (
  <MyNavScreen  navigation={navigation} />
);


const Profile = ({ navigation }) => (
  <MyNavScreen navigation={navigation} />
);

Profile.navigationOptions = {
  tabBarLabel: 'Profile',
};

const Middle = ({ navigation }) => (
  <UpcomingWorkouts enable={true} navigation={navigation} />
);

Middle.navigationOptions = {

  tabBarOptions : {showLabel:'false'}
};

const Notifications = ({ navigation }) => (
  <MyNavScreen navigation={navigation} />
);

Notifications.navigationOptions = {
  tabBarLabel: 'Notifications',
};

const More = ({ navigation }) => (
  <MyNavScreen navigation={navigation} />
);

Notifications.navigationOptions = {
  tabBarLabel: 'More',
};

export default  class AllComponents extends Component {
  constructor(props) {
  super(props);

  };


  render() {
    const {params} = this.props;
    return (

      <AllComponentsTab screenProps={params}/>

    );
  }
}

const AllComponentsTab = TabNavigator(
  {
    Feed: {
      screen: Feed,
      navigationOptions: {
      tabBarLabel: 'FEED',
      tabBarIcon:({ tintColor }) => <Image style={{ width: 25, height: 22 }} source={{uri:'feedicon'}} />
    },


    },
    Profile: {
      screen: Profile,
      navigationOptions: {
      tabBarLabel: 'PROFILE',
      tabBarIcon: ({ tintColor }) =><Image style={{ width: 25, height: 25 }} source={{uri:'profileicon'}} />
    },


    },
    Middle: {
      screen: Middle,
      navigationOptions: {
      tabBarLabel:' ',
      tabBarIcon: ({ tintColor }) =><Image style={{ width: 50, height: 50,paddingTop:10, position:'absolute', top:12, zIndex:999}} source={{uri:'bottom_tab_middlelogo'}} /> ,

    },

    },
    Notifications: {
      screen: Notifications,
      navigationOptions: {
      tabBarLabel: 'NOTIFICATIONS',
      tabBarIcon: ({ tintColor }) =><Image style={{ width: 27, height: 30}} source={{uri:'notificationicon'}} />
    },

    },
    More: {
      screen: More,
      navigationOptions: {
      tabBarLabel: 'MORE',
      tabBarIcon: ({ tintColor }) =><Image style={{ width: 25, height: 25}} source={{uri:'btnmenuicon'}} />
    },

    },

  },
  {
      tabBarOptions: {
        activeTintColor: '#FFF',  // Color of tab when pressed
        inactiveTintColor: '#FFF', // Color of tab when not pressed
        showIcon: 'true', // Shows an icon for both iOS and Android

        labelStyle: {
          fontSize: 9,
        },

        indicatorStyle :{
          backgroundColor: 'transparent',
          borderBottomWidth: 10
        },
        style: {
          backgroundColor: '#414249',
          borderTopWidth: 1,
          borderTopColor: 'transparent',
          position: 'absolute',
          left: 0,
          right: 0,
          bottom: 0,
          height:70

        }
      },
        tabBarComponent: TabBarBottom,
        tabBarPosition: 'bottom',
      }

);
