export const ENTRIES1 = [
    {
        title: 'Beautiful and dramatic Antelope Canyon',
        subtitle: 'Lorem ipsum dolor sit amet et nuncat mergitur',
        illustration: 'http://i.imgur.com/UYiroysl.jpg'
    },
    {
        title: 'Earlier this morning, NYC',
        subtitle: 'Lorem ipsum dolor sit amet',
        illustration: 'http://i.imgur.com/UPrs1EWl.jpg'
    },
    
];

export const ENTRIES2 = [
    {
        title: 'Favourites landscapes',
        subtitle: 'Lorem ipsum dolor sit amet',
        illustration: 'http://i.imgur.com/SsJmZ9jl.jpg'
    },
    {
        title: 'Favourites landscapes',
        subtitle: 'Lorem ipsum dolor sit amet et nuncat mergitur',
        illustration: 'http://i.imgur.com/5tj6S7Ol.jpg'
    },
    {
        title: 'Favourites landscapes',
        subtitle: 'Lorem ipsum dolor sit amet et nuncat',
        illustration: 'http://i.imgur.com/pmSqIFZl.jpg'
    },
    {
        title: 'Favourites landscapes',
        subtitle: 'Lorem ipsum dolor sit amet et nuncat mergitur',
        illustration: 'http://i.imgur.com/cA8zoGel.jpg'
    },
    {
        title: 'Favourites landscapes',
        subtitle: 'Lorem ipsum dolor sit amet',
        illustration: 'http://i.imgur.com/pewusMzl.jpg'
    },
    {
        title: 'Favourites landscapes',
        subtitle: 'Lorem ipsum dolor sit amet et nuncat',
        illustration: 'http://i.imgur.com/l49aYS3l.jpg'
    }
];
