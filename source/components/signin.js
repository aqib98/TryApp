/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Switch,
  Image,
  PixelRatio,
  KeyboardAvoidingView,
  ScrollView,
  TouchableHighlight,
  TouchableOpacity,
  TextInput

} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import Icon from 'react-native-vector-icons/FontAwesome';
import { NavigationActions } from 'react-navigation';

const Dimensions = require('Dimensions');
const {height, width} = Dimensions.get('window');

import SplashScreen from 'react-native-smart-splash-screen'
import styles from './styles.js'



export default class SignIn extends Component<{}> {
      constructor(props) {
      super(props);
      this.state = {
        width:0,
        height : 0
      };
    }

    componentDidMount () {


    }

    onPressChevronLeft = (navigate) => {
      const navigateAction = NavigationActions.navigate({

        routeName: 'Home',
        params: {'Home':'yes'},
        })
      navigate.dispatch(navigateAction)
    }

    onPressSignin = (navigate) => {
      const navigateAction = NavigationActions.navigate({

        routeName: 'Workout',
        params: {'Workout':'yes'},
        })
      navigate.dispatch(navigateAction)
    }

// <View>
//     <Icon name="chevron-left" size={30} color="#900" style={{marginLeft:16,marginTop:16,position:'absolute'}} />
// </View>
  render() {
    const {navigation} = this.props

    const resizeMode = 'center';
    const text = 'I am some centered text';
    const myIcon = (<Icon name="rocket" size={30} color="#900" />)
    console.log(styles)
    return (


      <View style={styles.mainBody}>
        <View style={styles.chevron_left_icon}>
          <TouchableOpacity onPress={()=>this.onPressChevronLeft(navigation )}>
            <Icon name="chevron-left" size={25} color="#FF7E00"   />
          </TouchableOpacity>
        </View>

        <View style={styles.header}>
              <Text style={styles.topSignupTxt}>
                Sign in
              </Text>
        </View>

        <ScrollView
          style={{
            marginTop : 20,
            marginBottom:0,
            paddingBottom:60,
            height:Dimensions.get('window').height,
            width:Dimensions.get('window').width
          }}

        >
        <View style={styles.body1}>
              <Text style={styles.forTxt}>For students,sign up using your school email</Text>
              <TextInput  placeholder="Email@school.edu" underlineColorAndroid='transparent' autoCorrect={false} placeholderTextColor='#626264' style={styles.textInput_login} />
              <TextInput  placeholder="Password" underlineColorAndroid='transparent' autoCorrect={false} placeholderTextColor='#626264' style={styles.textInput_login} />


            <Text style={styles.nonStudent}>Non students create an account using option below</Text>

              <TouchableOpacity  style={styles.submitFB}>

                <Icon name="facebook-f" size={30} color="#fff" style={{position:'absolute',left:25,top:17,fontSize:20}} />
                  <Text style={styles.buttonText}>
                    Sign up with Facebook
                  </Text>

              </TouchableOpacity>
              <TouchableOpacity  style={styles.submitTW}>

                <Icon name="twitter" size={30} color="#fff" style={{position:'absolute',left:25,top:17,fontSize:20}} />
                  <Text style={styles.buttonText}>
                    Sign up with Twitter
                  </Text>

              </TouchableOpacity>
              <TouchableOpacity  style={styles.submitGM}>

                <Icon name="google-plus" size={30} color="#fff" style={{position:'absolute',left:25,top:17,fontSize:20}} />
                  <Text style={styles.buttonText}>
                    Sign up with Gmail
                  </Text>

              </TouchableOpacity>
        </View>

        </ScrollView>


        <View style={styles.footer}>

            <TouchableOpacity onPress = {()=>this.onPressSignin(navigation )}>
              <View style={styles.save_view}>
                  <Text style={styles.save_btnTxt}>Sign in</Text>
              </View>

              <Image
                style={styles.save_btnImg}
                source={{ uri: 'buttonimg' }}
              />

            </TouchableOpacity>
          </View>

            </View>



    );
  }
}
